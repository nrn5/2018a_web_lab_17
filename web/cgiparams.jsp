<%@ page import="java.util.Map" %>
<%@ page import="java.util.Arrays" %><%--
  Created by IntelliJ IDEA.
  User: nrn5
  Date: 15/05/2018
  Time: 2:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Title</title>
    </head>
    <body>
        <table>
            <% Map<String, String[]> params = request.getParameterMap();
            for (String s : params.keySet()) { %>
            <tr><th><span><%= s %></span></th> - <td><span><%= Arrays.toString(params.get(s)) %></span></td></tr>
            <% } %>
        </table>
    </body>
</html>
