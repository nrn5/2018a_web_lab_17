<%@ page import="java.time.LocalDateTime" %><%--
  Created by IntelliJ IDEA.
  User: nrn5
  Date: 15/05/2018
  Time: 11:26 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>Exercise 01-02</title>
    </head>
    <body>

        <h1>Carpe Diem - Seize the Day</h1>

        <p>Ut finibus risus arcu, eget ullamcorper nunc convallis id. Aliquam tempor justo vitae convallis aliquam. Nam porttitor elit nec tellus iaculis congue. Aenean id congue tortor, et ornare nibh. Nulla in massa tincidunt, luctus ligula ut, aliquam diam. Phasellus sollicitudin eleifend leo, quis faucibus lectus lacinia id. Praesent lobortis interdum nunc a molestie. Nulla facilisi. Vestibulum imperdiet justo id sapien pharetra, at porta libero gravida. In in mi efficitur, sagittis tellus eu, suscipit felis. Etiam imperdiet feugiat vestibulum. Duis fringilla, ligula sed mattis sollicitudin, purus leo eleifend enim, ut laoreet ante massa at nibh. Nulla quis ante eu leo ultrices imperdiet. Sed magna tellus, facilisis nec felis vel, faucibus iaculis magna.</p>

        <p>Ut sollicitudin lacinia velit at consectetur. Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur nisi nibh, pulvinar quis feugiat at, fermentum vitae dolor. Phasellus lacinia, mi in maximus mattis, nibh diam tempus odio, vitae cursus quam turpis vel risus. Cras tempor sollicitudin urna non vulputate. Vivamus ornare libero nec lorem mattis, vitae congue augue venenatis. Vivamus ac ipsum accumsan, rutrum purus quis, placerat libero. Cras posuere erat vel nunc ullamcorper, placerat eleifend erat euismod. Quisque in ante ac lectus semper finibus eu ac eros. Duis risus quam, lacinia et posuere eu, mattis quis magna. Phasellus libero tellus, scelerisque quis leo at, ullamcorper laoreet massa. Duis efficitur non urna id semper. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>

        <p>Phasellus id mauris ut leo viverra egestas. Aenean condimentum egestas sollicitudin. Pellentesque dignissim leo nec ante vulputate venenatis. Vestibulum diam ex, imperdiet finibus porta id, viverra non nulla. Cras eleifend, justo aliquet volutpat luctus, quam erat egestas mi, in imperdiet magna nulla at est. Donec nec nulla ac sem pretium laoreet eget nec arcu. Duis et urna accumsan, vestibulum diam nec, suscipit nibh.</p>

        <p>Nulla id diam sed ligula aliquam euismod sit amet quis justo. Praesent facilisis egestas purus, quis condimentum diam gravida sed. Fusce ac augue mattis, fringilla eros in, condimentum odio. Ut et leo mauris. Vivamus dictum ut nunc a eleifend. Aliquam sed pretium arcu. Suspendisse tincidunt ante felis, eget aliquam est rhoncus sit amet. Aliquam erat volutpat. Curabitur maximus nec mi eget fringilla. Aliquam erat volutpat. Sed cursus tellus vitae neque efficitur malesuada. Proin hendrerit varius tellus, non iaculis velit faucibus sed. Maecenas id pretium sapien. Fusce interdum vestibulum nibh eget condimentum. Mauris enim est, sagittis vitae lorem vel, lobortis pharetra erat.</p>

        <p>Morbi fermentum leo sit amet nunc egestas posuere. Quisque condimentum mattis magna, non laoreet est posuere ac. Praesent felis nisl, volutpat sed semper a, lobortis non magna. Aliquam at convallis metus. Fusce et ipsum sit amet nibh porttitor scelerisque. Nulla facilisi. Vestibulum congue non ligula accumsan finibus. Sed a placerat orci, eget lacinia erat. In hac habitasse platea dictumst. Sed vulputate tristique libero tristique suscipit.</p>

        <hr>

        <% LocalDateTime localDateTime = LocalDateTime.now();
            out.print(localDateTime.toString()); %>

    </body>
</html>
